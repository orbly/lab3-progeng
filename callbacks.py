def write_button(app, acc):
    combo = app.combo.get()
    print(combo)
    value = int(app.entry.get())
    if combo == 'Доход':
        acc.set_income(value)
    else:
        acc.set_outcome(value)
    app.summary_label.config(text=f'{combo} суммой {str(value)} руб. записан')
    app.entry.delete(0, 'end')


def summary_button(app, acc):
    value = acc.get_summary()
    if value > 0:
        app.summary_label.config(background='green')
    elif value < 0:
        app.summary_label.config(background='red')
    app.summary_label.config(text=str(value)+' руб.')
    app.entry.delete(0, 'end')
