from tkinter import Tk
from application import *
from accountant import *
from callbacks import *


def assign_callbacks(app, acc):
    app.summary_button.config(command=lambda: summary_button(app, acc))
    app.write.config(command=lambda: write_button(app, acc))


def setup(root):
    app = Application(root)
    acc = Accountant()
    assign_callbacks(app, acc)


def main():
    root = Tk()
    setup(root)
    root.mainloop()


if __name__ == "__main__":
    main()
